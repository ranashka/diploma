# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''

import wx, sys
import Projects

class MyLeftCtrl(wx.ListCtrl):
    def __init__(self, parent, id, totalFile):
        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT)

        self.totalFile = totalFile
        self.id = id
        self.parent = parent
        self.project = Projects.ProjectList(totalFile)
        self.listOfProjects = self.project.projects
        self.InsertColumn(0, u'№', width=30)
        self.InsertColumn(1, 'Name', width=140)
        self.InsertColumn(2, 'Years', width=120)
        self.InsertColumn(3, 'Investments', wx.LIST_FORMAT_RIGHT, 90)

        for i in self.listOfProjects:
            index = self.InsertStringItem(sys.maxint, i[0])
            self.SetStringItem(index, 1, i[1])
            self.SetStringItem(index, 2, i[2])
            self.SetStringItem(index, 3, i[3])

    def deleteUnchosen(self, item):
        self.project.delUnchoice(item)

    def addUnchoice(self,item):
        self.project.addUnchoice(item)

    def updateCtrl(self):
        for i in self.totalFile.chosenProjects:
            index = self.InsertStringItem(sys.maxint, i[0])
            self.SetStringItem(index, 1, i[1])
            self.SetStringItem(index, 2, i[2])
            self.SetStringItem(index, 3, i[3])

    def delProjectAll(self, item):
        self.project.delProjectAll(item)


class MyRightCtrl(wx.ListCtrl):
    def __init__(self, parent, id, totalFile):
        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT)

        self.totalFile = totalFile
        self.id = id
        self.parent = parent
        self.project = Projects.ProjectList(totalFile)
        self.listChosenProject = self.project.chosenProjects
        self.InsertColumn(0, u'№', width=30)
        self.InsertColumn(1, 'Name', width=140)
        self.InsertColumn(2, 'Years', width=120)
        self.InsertColumn(3, 'Investments', wx.LIST_FORMAT_RIGHT, 90)

        for j in self.listChosenProject:
            index = self.InsertStringItem(sys.maxint, j[0])
            self.SetStringItem(index, 1, j[1])
            self.SetStringItem(index, 2, j[2])
            self.SetStringItem(index, 3, j[3])

    def addChoice(self, item):
        self.project.addChoice(item)

    def deleteChoice(self, item):
        self.project.delChoice(item)

    def updateCtrl(self):
        self.DeleteAllItems()

    def delProjectAll(self, item):
        self.project.delProjectAll(item)

class PortfolioCtrl(wx.ListCtrl):
    def __init__(self, parent, id, totalFile):
        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT)

        self.totalFile = totalFile
        self.id = id
        self.parent = parent
        self.projects = totalFile.portfolios

        self.InsertColumn(0, 'Portfolio', width=140)
        self.InsertColumn(1, 'Projects', width=140)
        self.InsertColumn(2, 'Amount', width=190)

        for i in self.projects:
            index = self.InsertStringItem(sys.maxint, i[0])
            self.SetStringItem(index, 1, i[1])
            self.SetStringItem(index, 2, i[2])

    def updateCtrl(self):
        self.__init__(self.parent, self.id, self.totalFile)

    def addPortfolio(self, item):
        index = self.InsertStringItem(sys.maxint, item[0])
        self.SetStringItem(index, 1, item[1])
        self.SetStringItem(index, 2, item[2])