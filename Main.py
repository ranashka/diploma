# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''


#just comment

import wx, sys
import Menus, ToolBar, Projects, MainPanel, Experts
import wx.lib.platebtn as pbtn
import wx.lib.agw.gradientbutton as gbtn

class AllPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

        self.parent = parent

        font = wx.Font(20, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        headersizer = wx.BoxSizer(wx.HORIZONTAL)
        panel1 = wx.Panel(self)
        curProjects = wx.StaticText(panel1, wx.ID_ANY, 'Current Projects', style = wx.ALIGN_CENTER)
        curProjects.SetFont(font)
        choProjects = wx.StaticText(panel1, wx.ID_ANY, 'Chosen Projects', style = wx.ALIGN_CENTER)
        choProjects.SetFont(font)
        headersizer.Add(curProjects, 1, wx.EXPAND)
        headersizer.Add(choProjects, 1, wx.EXPAND)
        panel1.SetSizer(headersizer)


        self.splitter = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D)

        p1 = MainPanel.MyLeftCtrl(self.splitter, -1)
        p2 = MainPanel.MyRightCtrl(self.splitter, -1)
        self.splitter.SplitVertically(p1, p2, 400)

        panel2 = wx.Panel(self)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        button1 = wx.Button(panel2,-1, 'Text')
        button1.Bind(wx.EVT_BUTTON, self.onButton)
        button2 = gbtn.GradientButton(panel2, label = 'GradienrButton')
        buttonSizer.Add(button1, 0)
        buttonSizer.Add(button2, 1, wx.EXPAND)
        panel2.SetSizer(buttonSizer)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(panel1, 0, flag = wx.ALL|wx.EXPAND)
        self.sizer.Add(self.splitter,1, wx.EXPAND)
        self.sizer.Add(panel2,1,flag = wx.ALL|wx.EXPAND)
        self.SetSizer(self.sizer)
        self.Layout()

    def onButton(self, event):
        self.parent.Sizer.Hide(0)
        self.parent.Sizer.Show(1)
        event.Skip()

class Example(wx.Frame):
    def __init__(self, *args, **kwargs):
        self.width = 800
        wx.Frame.__init__(self, None, title="Nested Splitters",
                           size=(self.width,600))
        self.InitUI()

    def InitUI(self):
        self.panel = wx.Panel(self)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.AddingWindows()
        self.sizer.Show(0)
        Menus.MenuBar(self)
        ToolBar.ToolBar(self)
        self.statusBar = self.CreateStatusBar()
        self.statusBar.SetStatusText('Ready')
        self.Bind(wx.EVT_PAINT, self.Paint)
        # self.SetSize((600, 600))
        self.SetTitle('ProgRa')
        self.Centre()

    def Paint(self, e):
        self.panel.splitter.SetSashPosition(int((self.width)/2))
        self.Update()

    def AddingWindows(self):
        self.mainPanel = AllPanel(self.panel)
        self.sizer.Add(self.mainPanel, 1, wx.EXPAND)
        self.sizer.Hide(0)

        self.expertPanel = Experts.ExpertsPanel(self.panel)
        self.sizer.Add(self.mainPanel, 1, wx.EXPAND)
        self.sizer.Hide(1)

def main():
    ex = wx.App()
    frame = Example()
    frame.Show()
    ex.MainLoop()

if __name__ == '__main__':
    main()