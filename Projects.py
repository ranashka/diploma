# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''

import wx
import codecs
import sys

class ProjectList:
    def __init__(self, totalFile):
        self.totalFile = totalFile
        self.projects = self.totalFile.unchosenProjects
        self.chosenProjects = self.totalFile.chosenProjects
        self.portfolios = self.totalFile.portfolios
        self.evoluations = self.totalFile.evoluations
        self.risks = self.totalFile.risks

    def getChosenSum(self):
        summa = 0
        for i in self.chosenProjects:
            summa += float(i[3])
        return summa

    def addChoice(self, item):
        self.chosenProjects.append(((item[0], item[1], item[2], item[3])))
    #
    def addUnchoice(self, item):
        self.projects.append(((item[0], item[1], item[2], item[3])))

    def delChoice(self, item):
        counter = 0
        for i in self.chosenProjects:
            if i[0] == item:
                del self.chosenProjects[counter]
            counter +=1

    def delUnchoice(self, item):
        counter = 0
        for i in self.projects:
            if i[0] == item:
                del self.projects[counter]
            counter +=1

    def delProjectAll(self, item):
        counter = 0
        for i in self.chosenProjects:
            if i[0] == item:
                del self.chosenProjects[counter]
            counter +=1
        counter = 0
        for i in self.projects:
            if i[0] != item:
                del self.projects[counter]
            counter +=1
        tempDict = []
        counter = 0
        for i in self.evoluations:
            if i[1] == item:
                tempDict.append(self.evoluations[counter])
            counter +=1
        del self.evoluations
        self.evoluations = tempDict
        tempDict2 = []
        counter = 0
        for i in self.risks:
            if i[1] == item:
                tempDict2.append(self.risks[counter])
            counter +=1
        del self.risks
        self.risks = tempDict2


    def delUnchoice(self, item):
        counter = 0
        for i in self.projects:
            if i[0] == item:
                del self.projects[counter]
            counter +=1


    def onSave(self):
        self.totalFile.chosenProjects = self.chosenProjects
        self.totalFile.unchosenProjects = self.projects

class ProjectInfoDialog(wx.Frame):
    def __init__(self, allFile, panel, *args, **kwargs):
        self.width = 400
        self.allFile = allFile
        self.New = True
        wx.Frame.__init__(self, wx.GetApp().TopWindow, title='Projects',
                      size = (self.width,350))

        self.InitUI(allFile, panel)

    def InitUI(self, allFile, panel):

        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        panel1 = wx.Panel(pnl)
        headerSizer = wx.BoxSizer(wx.HORIZONTAL)
        font = wx.Font(16, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        expertHeader = wx.StaticText(panel1, wx.ID_ANY, 'Add project information', style = wx.ALIGN_CENTER)
        expertHeader.SetFont(font)
        headerSizer.Add(expertHeader, 1, wx.EXPAND)
        panel1.SetSizer(headerSizer)

        panel2 = wx.Panel(pnl)
        nameSizer = wx.BoxSizer(wx.HORIZONTAL)
        font2 = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        nameHeader = wx.StaticText(panel2, -1, 'Name', style = wx.ALIGN_LEFT, size = wx.Size(150, 20))
        nameHeader.SetFont(font2)
        self.nameInput = wx.TextCtrl(panel2, -1, style = wx.ALIGN_RIGHT)
        self.nameInput.SetFont(font2)
        nameSizer.Add(nameHeader, 0)
        nameSizer.Add(self.nameInput, 1, wx.EXPAND)
        panel2.SetSizer(nameSizer)

        panel3 = wx.Panel(pnl)
        investSizer = wx.BoxSizer(wx.HORIZONTAL)
        investHeader = wx.StaticText(panel3, -1, 'Investments', style = wx.ALIGN_LEFT, size = wx.Size(150, 20))
        investHeader.SetFont(font2)
        self.investInput = wx.TextCtrl(panel3, -1, style = wx.ALIGN_RIGHT)
        self.investInput.SetFont(font2)
        investSizer.Add(investHeader, 0)
        investSizer.Add(self.investInput, 1, wx.EXPAND)
        panel3.SetSizer(investSizer)

        panel4 = wx.Panel(pnl)
        yearSizer = wx.BoxSizer(wx.HORIZONTAL)
        yearHeader = wx.StaticText(panel4, -1, 'Year', style = wx.ALIGN_LEFT, size = wx.Size(150, 20))
        yearHeader.SetFont(font2)
        self.yearInput = wx.TextCtrl(panel4, -1, style = wx.ALIGN_RIGHT)
        self.yearInput.SetFont(font2)
        yearSizer.Add(yearHeader, 0)
        yearSizer.Add(self.yearInput, 1, wx.EXPAND)
        panel4.SetSizer(yearSizer)

        panel5 = wx.Panel(pnl)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        ButOK = wx.Button(panel5, -1, 'OK')
        ButApply = wx.Button(panel5, -1, 'Apply')
        ButCancel = wx.Button(panel5, -1, 'Cancel')
        ButOK.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButApply.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButCancel.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButOK.Bind(wx.EVT_BUTTON, lambda event: self.OnOK(event, allFile, panel))
        ButApply.Bind(wx.EVT_BUTTON, lambda event: self.OnApply(event, allFile, panel))
        ButCancel.Bind(wx.EVT_BUTTON, self.OnCancel)
        buttonSizer.Add(ButOK, 1, wx.ALL, 5)
        buttonSizer.Add(ButApply, 1, wx.ALL, 5)
        buttonSizer.Add(ButCancel, 1, wx.ALL, 5)
        panel5.SetSizer(buttonSizer)

        vbox.Add(panel1, 0, wx.EXPAND|wx.ALL, 15)
        vbox.Add(panel2, 0, wx.EXPAND|wx.ALL, 15)
        vbox.Add(panel3, 0, wx.EXPAND|wx.ALL, 15)
        vbox.Add(panel4, 0, wx.EXPAND|wx.ALL, 15)
        vbox.Add(panel5, 1, wx.EXPAND|wx.ALL, 15)

        pnl.SetSizer(vbox)
        pnl.Layout()

        if panel.selectedProject != '':
            projectInfo = allFile.ProjectInfo(panel.selectedProject)
            self.nameInput.Value = projectInfo[1]
            self.investInput.Value = projectInfo[3]
            self.yearInput.Value = projectInfo[2]
            self.New = False

    def OnClose(self, e):
        self.Destroy()

    def AddProject(self, allFile, panel):
        item = []
        maxID = 0
        for project in allFile.chosenProjects:
            if maxID < project[0]:
                maxID = project[0]
        for project in allFile.unchosenProjects:
            if maxID < project[0]:
                maxID = project[0]
        item.append(str(int(maxID) + 1))
        item.append(unicode(self.nameInput.Value))
        item.append(unicode(self.yearInput.Value))
        item.append(unicode(self.investInput.Value) + '\r\n')
        allFile.unchosenProjects.append(((item[0], item[1], item[2], item[3])))
        index = panel.p1.InsertStringItem(sys.maxint, str(item[0]))
        panel.p1.SetStringItem(index, 1, item[1])
        panel.p1.SetStringItem(index, 2, item[2])
        panel.p1.SetStringItem(index, 3, item[3])

    def UpdateProject(self, allFile, panel):
        allFile.DelProjectForChange(panel.selectedProject)
        self.item = []
        self.item.append(str(panel.selectedProject))
        self.item.append(unicode(self.nameInput.Value))
        self.item.append(unicode(self.yearInput.Value))
        self.item.append(unicode(self.investInput.Value) + '\r\n')
        allFile.unchosenProjects.append(((self.item[0], self.item[1], self.item[2], self.item[3])))

    def OnOK(self, e, allFile, panel):
        if (self.nameInput.Value + self.yearInput.Value + self.investInput.Value).strip() and self.New:
            self.AddProject(allFile, panel)
        elif (self.nameInput.Value + self.yearInput.Value + self.investInput.Value).strip():
            self.UpdateProject(allFile, panel)
            if panel.LeftFocus:
                index = panel.p1.InsertStringItem(sys.maxint, str(self.item[0]))
                panel.p1.SetStringItem(index, 1, self.item[1])
                panel.p1.SetStringItem(index, 2, self.item[2])
                panel.p1.SetStringItem(index, 3, self.item[3])
                panel.p1.DeleteItem(panel.selectedItem)
            else:
                index = panel.p2.InsertStringItem(sys.maxint, str(self.item[0]))
                panel.p2.SetStringItem(index, 1, self.item[1])
                panel.p2.SetStringItem(index, 2, self.item[2])
                panel.p2.SetStringItem(index, 3, self.item[3])
                panel.p2.DeleteItem(panel.selectedItem)
        self.Destroy()

    def OnApply(self, e, allFile,panel):
        if (self.nameInput.Value + self.yearInput.Value + self.investInput.Value).strip()and self.New:
            self.AddProject(allFile, panel)
        elif (self.nameInput.Value + self.yearInput.Value + self.investInput.Value).strip():
            self.UpdateProject(allFile, panel)
            if panel.LeftFocus:
                index = panel.p1.InsertStringItem(sys.maxint, str(self.item[0]))
                panel.p1.SetStringItem(index, 1, self.item[1])
                panel.p1.SetStringItem(index, 2, self.item[2])
                panel.p1.SetStringItem(index, 3, self.item[3])
                panel.p1.DeleteItem(panel.selectedItem)
            else:
                index = panel.p2.InsertStringItem(sys.maxint, str(self.item[0]))
                panel.p2.SetStringItem(index, 1, self.item[1])
                panel.p2.SetStringItem(index, 2, self.item[2])
                panel.p2.SetStringItem(index, 3, self.item[3])
                panel.p2.DeleteItem(panel.selectedItem)
        self.nameInput.Value = ''
        self.yearInput.Value = ''
        self.investInput.Value = ''

    def OnCancel(self, e):
        self.Destroy()