import wx

def OnOpen(self, event):
    if self.contentNotSaved:

        # if wx.MessageBox("Current content has not been saved! Proceed?", "Please confirm",
        #                  wx.ICON_QUESTION | wx.YES_NO, self) == wx.NO:
        #     return

        # else: proceed asking to the user the new file to open

        openFileDialog = wx.FileDialog(self, "Open RNA file", "", "",
                                       "RNA files (*.rna)|*.rna", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if openFileDialog.ShowModal() == wx.ID_CANCEL:
            return self.inFile

        try:
            path = openFileDialog.GetPath()
        except:
            wx.LogError("Cannot open file '%s'."%openFileDialog.GetPath())
            path = ''
            return path
        finally:
            return path

def OnSave(self, e):

    if wx.MessageBox("Do you want to save the project?", "Please confirm", wx.ICON_QUESTION | wx.YES_NO, self) == wx.NO:
        return self.inFile

    else:
        saveFileDialog = wx.FileDialog(self, "Save RNA file", "", "",
                                   "RNA files (*.rna)|*.rna", wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

        if saveFileDialog.ShowModal() == wx.ID_CANCEL:
            return     # the user changed idea...

        try:
            path = saveFileDialog.GetPath()
        except:
            wx.LogError("Cannot open file '%s'."%saveFileDialog.GetPath())
            path = ''
        finally:
            return path

def OnSaveAs(self, e):
    if self.panel.p1.GetItemCount() + self.panel.p2.GetItemCount() == 0:
        return self.inFile
    if 'Temp' in self.inFile:
        return OnSave(self, e)
    else:
        return self.inFile