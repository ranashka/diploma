import wx

import Menus

class MyForm(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "Tutorial")

        Menus.MenuBar(self)


        # create the main sizer
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        # Add a panel so it looks the correct on all platforms
        self.panel = wx.Panel(self, wx.ID_ANY)

        lbls = ["labelOne", "lblTwo", "lblThree"]
        for lbl in lbls:
            self.buildLayout(lbl)
        self.panel.SetSizer(self.mainSizer)

    #----------------------------------------------------------------------
    def buildLayout(self, text):
        """"""
        lblSize = (60,-1)
        lbl = wx.StaticText(self.panel, label=text, size=lblSize)
        txt = wx.TextCtrl(self.panel)

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(lbl, 0, wx.ALL|wx.ALIGN_LEFT, 5)
        sizer.Add(txt, 0, wx.ALL, 5)
        self.mainSizer.Add(sizer)

# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyForm()
    frame.Show()
    app.MainLoop()