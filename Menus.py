# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''

import wx
import OpenSave

class MenuBar(wx.MenuBar):
    def __init__(self, parent):
        super(MenuBar, self).__init__()

        self.parent = parent
        self.Menus()

    def Menus(self):
        ID_TOOLBAR = wx.ID_ANY
        ID_STATUSBAR = wx.ID_ANY

        ID_PROJECT = wx.ID_ANY
        ID_EXPERT = wx.ID_ANY

        ID_NPV = wx.ID_ANY

        menubar = wx.MenuBar()

        fileMenu = wx.Menu()
        new = fileMenu.Append(wx.ID_NEW, '&New\tCtrl+N')
        fopen = fileMenu.Append(wx.ID_OPEN, '&Open\tCtrl+O')
        save = fileMenu.Append(wx.ID_SAVE, '&Save\tCtrl+S')
        fileMenu.AppendSeparator()
        qmi = fileMenu.Append(wx.ID_EXIT, '&Quit\tCtrl+W')

        viewMenu = wx.Menu()
        viewMenu.Append(ID_TOOLBAR, 'Show &toolbar')
        viewMenu.Append(ID_STATUSBAR, 'Show &statusbar')

        toolMenu = wx.Menu()
        toolMenu.Append(ID_PROJECT, '&Project')
        toolMenu.Append(ID_EXPERT, '&Expert')
        graphMenu = wx.Menu()
        graphMenu.Append(ID_NPV, '&NPV')
        graphMenu.Append(ID_NPV, '&IRR')
        graphMenu.Append(ID_NPV, '&PI')
        graphMenu.Append(ID_NPV, '&PP')
        toolMenu.AppendMenu(wx.ID_ANY, '&Graph',graphMenu)

        self.parent.Bind(wx.EVT_MENU, self.OnClose, qmi)
        self.parent.Bind(wx.EVT_MENU, self.OnOpen, fopen)
        self.parent.Bind(wx.EVT_MENU, self.OnNew, new)
        self.parent.Bind(wx.EVT_MENU, self.OnSave, save)

        menubar.Append(fileMenu, '&File')
        menubar.Append(viewMenu, '&View')
        menubar.Append(toolMenu, '&Tool')

        self.parent.SetMenuBar(menubar)

    def OnClose(self, e):
        self.parent.OnClose(e)

    def OnOpen(self, e):
        path = OpenSave.OnSaveAs(self.parent, e)
        self.parent.allFile.writeFile(path)
        path = OpenSave.OnOpen(self.parent, e)
        self.parent.OnOpen(path)

    def OnSave(self, e):
        path = OpenSave.OnSaveAs(self.parent, e)
        self.parent.allFile.writeFile(path)

    def OnNew(self, e):
        path = OpenSave.OnSaveAs(self.parent, e)
        self.parent.allFile.writeFile(path)
        self.parent.OnNew(e)

