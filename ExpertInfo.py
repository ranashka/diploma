# -*- coding: utf-8 -*-
import wx, sys
from Expert_h import Expert

class ExpertInfoDialog(wx.Frame):
    def __init__(self, allFile, panel, expertID = '0', itemToDel = '0', *args, **kwargs):
        self.width = 1200
        self.expertID = expertID
        self.itemToDel = itemToDel
        wx.Frame.__init__(self, wx.GetApp().TopWindow, title='ProjectEvoluation',
                          size = (self.width,400))

        self.InitUI(allFile, panel)


    def InitUI(self, allFile, panel):

        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        panel1 = wx.Panel(pnl)
        headerSizer = wx.BoxSizer(wx.HORIZONTAL)
        font = wx.Font(20, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        expertHeader = wx.StaticText(panel1, wx.ID_ANY, 'Add expert information', style = wx.ALIGN_CENTER)
        expertHeader.SetFont(font)
        headerSizer.Add(expertHeader, 1, wx.EXPAND)
        panel1.SetSizer(headerSizer)

        self.splitter = wx.SplitterWindow(pnl, wx.ID_ANY, style=wx.SP_3D)
        panel2 = wx.Panel(self.splitter)
        fioSizer = wx.BoxSizer(wx.VERTICAL)

        panel21 = wx.Panel(panel2)
        nameSizer = wx.BoxSizer(wx.HORIZONTAL)
        lineFIO = allFile.getExpertFIO(self.expertID)
        if lineFIO == '':
            lineFIO = ['', '', '', '']

        font2 = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        nameHeader = wx.StaticText(panel21, -1, 'Name', style = wx.ALIGN_LEFT, size = wx.Size(150, 20))
        nameHeader.SetFont(font2)
        self.nameInput = wx.TextCtrl(panel21, -1, style = wx.ALIGN_RIGHT, value = lineFIO[0] )
        self.nameInput.SetFont(font2)
        nameSizer.Add(nameHeader, 0)
        nameSizer.Add(self.nameInput, 1, wx.EXPAND)
        panel21.SetSizer(nameSizer)

        panel22 = wx.Panel(panel2)
        surnameSizer = wx.BoxSizer(wx.HORIZONTAL)
        surnameHeader = wx.StaticText(panel22, -1, 'Surname', style = wx.ALIGN_LEFT, size = wx.Size(150, 20))
        surnameHeader.SetFont(font2)
        self.surnameInput = wx.TextCtrl(panel22, -1, style = wx.ALIGN_RIGHT, value = lineFIO[1])
        self.surnameInput.SetFont(font2)
        surnameSizer.Add(surnameHeader, 0)
        surnameSizer.Add(self.surnameInput, 1, wx.EXPAND)
        panel22.SetSizer(surnameSizer)

        panel23 = wx.Panel(panel2)
        midnameSizer = wx.BoxSizer(wx.HORIZONTAL)
        midnameHeader = wx.StaticText(panel23, -1, 'Middle name', style = wx.ALIGN_LEFT, size = wx.Size(150, 20))
        midnameHeader.SetFont(font2)
        self.midnameInput = wx.TextCtrl(panel23, -1, style = wx.ALIGN_RIGHT, value = lineFIO[2])
        self.midnameInput.SetFont(font2)
        midnameSizer.Add(midnameHeader, 0)
        midnameSizer.Add(self.midnameInput, 1, wx.EXPAND)
        panel23.SetSizer(midnameSizer)

        panel24 = wx.Panel(panel2)
        evolSizer = wx.BoxSizer(wx.HORIZONTAL)
        evolHeader = wx.StaticText(panel24, -1, 'Level of \n competence', style = wx.ALIGN_LEFT, size = wx.Size(150, 40))
        evolHeader.SetFont(wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD))
        self.evolInput = wx.TextCtrl(panel24, -1, style = wx.ALIGN_RIGHT, value = lineFIO[3])
        self.evolInput.SetFont(font2)
        evolSizer.Add(evolHeader, 0)
        evolSizer.Add(self.evolInput, 1)
        panel24.SetSizer(evolSizer)

        fioSizer.Add(panel21, 0, wx.EXPAND|wx.ALL, 15)
        fioSizer.Add(panel22, 0, wx.EXPAND|wx.ALL, 15)
        fioSizer.Add(panel23, 0, wx.EXPAND|wx.ALL, 15)
        fioSizer.Add(panel24, 1, wx.EXPAND|wx.ALL, 15)
        panel2.SetSizer(fioSizer)

        panelAbove3=wx.Panel(self.splitter)
        largeSizer = wx.BoxSizer(wx.HORIZONTAL)

        panelComboBoxes = wx.Panel(panelAbove3)
        comboBoxSizer = wx.BoxSizer(wx.VERTICAL)
        projects = []
        for pr in allFile.chosenProjects:
            projects.append(pr[1])
        for pr in allFile.unchosenProjects:
            projects.append(pr[1])

        fontCombo = wx.Font(11, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        comboTProject = wx.StaticText(panelComboBoxes, -1, 'Choose project', style = wx.ALIGN_CENTER)
        comboTProject.SetFont(fontCombo)
        self.comboProject = wx.ComboBox(panelComboBoxes, -1, choices=sorted(projects), name='Choose project', size = (150, 20))
        self.comboProject.SetFont(wx.Font(9, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL))
        comboTScenario = wx.StaticText(panelComboBoxes, -1, 'Choose scenario', style = wx.ALIGN_CENTER)
        comboTScenario.SetFont(fontCombo)
        self.comboScenario = wx.ComboBox(panelComboBoxes, -1, choices=['optimistic', 'pessimistic','realistic'], name='Choose scenario')
        self.comboScenario.SetFont(fontCombo)
        comboTYear = wx.StaticText(panelComboBoxes, -1, 'Choose year', style = wx.ALIGN_CENTER)
        comboTYear.SetFont(fontCombo)
        self.comboYear = wx.ComboBox(panelComboBoxes, -1, choices=['1', '2', '3', '4', '5'], name='Choose scenario')
        self.comboYear.SetFont(fontCombo)
        comboTMin = wx.StaticText(panelComboBoxes, -1, 'Enter min value', style = wx.ALIGN_CENTER)
        comboTMin.SetFont(fontCombo)
        self.valueMin = wx.TextCtrl(panelComboBoxes, -1, style = wx.ALIGN_RIGHT)
        self.valueMin.SetFont(fontCombo)
        comboTMax = wx.StaticText(panelComboBoxes, -1, 'Enter max value', style = wx.ALIGN_CENTER)
        comboTMax.SetFont(fontCombo)
        self.valueMax = wx.TextCtrl(panelComboBoxes, -1, style = wx.ALIGN_RIGHT)
        self.valueMax.SetFont(fontCombo)

        comboBoxSizer.Add(comboTProject, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(self.comboProject, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(comboTScenario, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(self.comboScenario, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(comboTYear, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(self.comboYear, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(comboTMin, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(self.valueMin, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(comboTMax, 0, wx.EXPAND|wx.ALL, 2)
        comboBoxSizer.Add(self.valueMax, 0, wx.EXPAND|wx.ALL, 2)

        panelComboBoxes.SetSizer(comboBoxSizer)

        ProjectInfoSizer = wx.BoxSizer(wx.VERTICAL)
        panel3 = wx.Panel(panelAbove3)
        panel31 = wx.Panel(panel3)
        prInfoSizer = wx.BoxSizer(wx.HORIZONTAL)
        projectInfo = wx.StaticText(panel31, -1, 'Project information', style = wx.ALIGN_CENTER)
        projectInfo.SetFont(font2)
        prInfoSizer.Add(projectInfo, 1, wx.EXPAND)
        panel31.SetSizer(prInfoSizer)

        panel32 = wx.Panel(panel3)
        projInfoSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.projectList = wx.ListCtrl(panel32, -1, style=wx.LC_REPORT)
        self.projectList.InsertColumn(0, 'Name', width=110)
        self.projectList.InsertColumn(1, 'Scenario', width=40)
        self.projectList.InsertColumn(2, 'Year1', width=110)
        self.projectList.InsertColumn(3, 'Year2', width=110)
        self.projectList.InsertColumn(4, 'Year3', width=110)
        self.projectList.InsertColumn(5, 'Year4', width=110)
        self.projectList.InsertColumn(6, 'Year5', width=110)

        listic = allFile.getExprertInfo(self.expertID)

        for item in listic:
            index = self.projectList.InsertStringItem(sys.maxint, item[0])
            for colomn in range(len(item)):
                self.projectList.SetStringItem(index, colomn, item[colomn].strip())

        projInfoSizer.Add(self.projectList, 1, wx.EXPAND, 10)
        panel32.SetSizer(projInfoSizer)

        panel33 = wx.Panel(panel3)
        butSizer = wx.BoxSizer(wx.HORIZONTAL)
        ButAdd = wx.Button(panel33, -1, 'Add')
        ButAdd.Bind(wx.EVT_BUTTON, lambda event: self.OnAdd(event, allFile))
        ButDel = wx.Button(panel33, -1, 'Delete')
        ButAdd.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButDel.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButDel.Bind(wx.EVT_BUTTON, lambda event: self.OnDel(event, allFile))
        butSizer.Add(ButAdd, 0, wx.ALL,2)
        butSizer.Add(ButDel, 0, wx.ALL,2)
        panel33.SetSizer(butSizer)

        ProjectInfoSizer.Add(panel31, 0, wx.ALIGN_CENTRE|wx.ALL, 15)
        ProjectInfoSizer.Add(panel32, 0, wx.ALIGN_CENTRE|wx.EXPAND)
        ProjectInfoSizer.Add(panel33, 0, wx.ALIGN_CENTRE)
        panel3.SetSizer(ProjectInfoSizer)

        largeSizer.Add(panelComboBoxes, 0, wx.ALIGN_CENTRE|wx.ALL)
        largeSizer.Add(panel3, 1,wx.ALIGN_CENTRE|wx.EXPAND|wx.ALL, 15)
        panelAbove3.SetSizer(largeSizer)

        self.splitter.SplitVertically(panel2, panelAbove3, 450)

        panel4 = wx.Panel(pnl)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        ButOK = wx.Button(panel4, -1, 'OK')
        ButApply = wx.Button(panel4, -1, 'Apply')
        ButCancel = wx.Button(panel4, -1, 'Cancel')
        ButOK.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButApply.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButCancel.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButOK.Bind(wx.EVT_BUTTON, lambda event: self.OnOK(event, allFile, panel))
        ButApply.Bind(wx.EVT_BUTTON, lambda event: self.OnApply(event, allFile, panel))
        ButCancel.Bind(wx.EVT_BUTTON, self.OnCancel)
        buttonSizer.Add(ButOK, 1, wx.ALL, 5)
        buttonSizer.Add(ButApply, 1, wx.ALL, 5)
        buttonSizer.Add(ButCancel, 1, wx.ALL, 5)
        panel4.SetSizer(buttonSizer)

        vbox.Add(panel1, 0, wx.ALIGN_CENTER)
        vbox.Add(self.splitter, 0, wx.EXPAND|wx.ALIGN_CENTER)
        vbox.Add(panel4, 1, wx.ALIGN_CENTER)

        pnl.SetSizer(vbox)
        pnl.Layout()

    def OnClose(self, e):
        self.Destroy()

    def OnAdd(self, e, allFile):
        project = self.comboProject.GetValue()
        scenario = self.comboScenario.GetValue()[:1].upper()
        year = self.comboYear.GetValue()
        minim = self.valueMin.GetValue()
        maxim = self.valueMax.GetValue()

        gotElement = False
        for line in range(self.projectList.GetItemCount()):
            if self.projectList.GetItemText(line, 0) == project and self.projectList.GetItemText(line, 1) == scenario:
                self.projectList.SetStringItem(line, int(year)+1, str(minim)+'-'+str(maxim))
                gotElement = True
        if not gotElement:
            index = self.projectList.InsertStringItem(sys.maxint, project)
            self.projectList.SetStringItem(index, 1, scenario)
            self.projectList.SetStringItem(index, int(year)+1, str(minim)+'-'+str(maxim))

    def OnDel(self, e, allFile):
        item = self.projectList.GetFocusedItem()
        scenario = self.projectList.GetItemText(item, 1)
        project = allFile.IDOfProject(self.projectList.GetItemText(item, 0))
        allFile.evoluationsToDel.append((self.expertID, project, scenario))
        self.projectList.DeleteItem(item)

    def AddExpert(self, allFile, panel):
        item = []
        maxID = 0
        for expert in allFile.experts:
            if maxID < expert[0]:
                maxID = int(expert[0])
        for expert in allFile.expertsToAdd:
            if maxID < expert[0]:
                maxID = int(expert[0])
        item.append(str(maxID + 1))
        item.append(unicode(self.nameInput.Value))
        item.append(unicode(self.surnameInput.Value))
        item.append(unicode(self.midnameInput.Value))
        item.append(unicode(self.evolInput.Value) + '\r\n')
        allFile.expertsToAdd.append(((item[0], item[1], item[2], item[3], item[4])))

        for line in range(self.projectList.GetItemCount()):
            project = allFile.IDOfProject(self.projectList.GetItemText(line, 0))
            scenario = self.projectList.GetItemText(line, 1)
            year1 = self.projectList.GetItemText(line, 2)
            year2 = self.projectList.GetItemText(line, 3)
            year3 = self.projectList.GetItemText(line, 4)
            if self.projectList.GetItemText(line, 5):
                year4 = self.projectList.GetItemText(line, 5)
                if self.projectList.GetItemText(line, 6):
                    year5 = self.projectList.GetItemText(line, 6) + '\r\n'
                    allFile.evoluationsToAdd.append((str(maxID + 1), project, scenario, year1, year2, year3, year4, year5))
                else:
                    year4 += '\r\n'
                    allFile.evoluationsToAdd.append((str(maxID + 1), project, scenario, year1, year2, year3, year4))
            else:
                year3 += '\r\n'
                allFile.evoluationsToAdd.append((str(maxID + 1), project, scenario, year1, year2, year3))

        index = panel.p1.InsertStringItem(sys.maxint, str(item[0]))
        panel.p1.SetStringItem(index, 1, item[1])
        panel.p1.SetStringItem(index, 2, item[2])
        panel.p1.SetStringItem(index, 3, item[3])
        panel.p1.SetStringItem(index, 4, item[4])


    def updateInfo(self, allFile, panel):

        allFile.delExpert(self.expertID)

        item = []
        item.append(str(self.expertID))
        item.append(unicode(self.nameInput.Value))
        item.append(unicode(self.surnameInput.Value))
        item.append(unicode(self.midnameInput.Value))
        item.append(unicode(self.evolInput.Value) + '\r\n')
        allFile.expertsToAdd.append(((item[0], item[1], item[2], item[3], item[4])))

        for line in range(self.projectList.GetItemCount()):
            project = allFile.IDOfProject(self.projectList.GetItemText(line, 0))
            scenario = self.projectList.GetItemText(line, 1)
            year1 = self.projectList.GetItemText(line, 2)
            year2 = self.projectList.GetItemText(line, 3)
            year3 = self.projectList.GetItemText(line, 4)
            if self.projectList.GetItemText(line, 5):
                year4 = self.projectList.GetItemText(line, 5)
                if self.projectList.GetItemText(line, 6):
                    year5 = self.projectList.GetItemText(line, 6) + '\r\n'
                    allFile.evoluationsToAdd.append((self.expertID, project, scenario, year1, year2, year3, year4, year5))
                else:
                    year4 += '\r\n'
                    allFile.evoluationsToAdd.append((self.expertID, project, scenario, year1, year2, year3, year4))
            else:
                year3 += '\r\n'
                allFile.evoluationsToAdd.append((self.expertID, project, scenario, year1, year2, year3))

        panel.p1.DeleteItem(self.itemToDel)
        index = panel.p1.InsertStringItem(sys.maxint, str(item[0]))
        panel.p1.SetStringItem(index, 1, item[1])
        panel.p1.SetStringItem(index, 2, item[2])
        panel.p1.SetStringItem(index, 3, item[3])
        panel.p1.SetStringItem(index, 4, item[4])

    def OnOK(self, e, allFile, panel):
        if self.expertID == '0' and (self.nameInput.Value + self.surnameInput.Value + self.midnameInput.Value + self.evolInput.Value).strip():
            self.AddExpert(allFile, panel)
        else:
            if (self.nameInput.Value + self.surnameInput.Value + self.midnameInput.Value + self.evolInput.Value).strip():
                self.updateInfo(allFile, panel)
        self.Destroy()

    def OnApply(self, e, allFile, panel):
        if (self.nameInput.Value + self.surnameInput.Value + self.midnameInput.Value + self.evolInput.Value).strip():
            if self.expertID == '0':
                self.AddExpert(allFile, panel)
            else:
                self.updateInfo(allFile, panel)
            self.nameInput.Value = ''
            self.surnameInput.Value = ''
            self.midnameInput.Value = ''
            self.evolInput.Value = ''
            self.projectList.DeleteAllItems()
            self.comboYear.Value = ''
            self.comboScenario.Value = ''
            self.comboProject.Value = ''
            self.valueMax.Value = ''
            self.valueMin.Value = ''

    def OnCancel(self, e):
        self.Destroy()