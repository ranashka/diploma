# -*- coding: utf-8 -*-

import wx, sys
import Menus, ToolBar, Projects, MainPanel
from Expert_h import Expert
from ExpertInfo import ExpertInfoDialog

class PortfolioCtrl(wx.ListCtrl):
    def __init__(self, parent, id, allFile):
        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT)

        self.parent = parent
        self.portfolios = allFile.portfolios

        self.InsertColumn(0, 'Portfolio', width=100)
        self.InsertColumn(1, 'Projects', width=100)
        self.InsertColumn(2, 'Amount', wx.LIST_FORMAT_RIGHT, 100)

        for i in self.portfolios:
            index = self.InsertStringItem(sys.maxint, i[0])
            self.SetStringItem(index, 1, i[1])
            self.SetStringItem(index, 2, i[2])

class HeadPanel(wx.Panel):
    def __init__(self, parent, allFile):
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        self.parent = parent

        font = wx.Font(20, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)

        expertHeaderSizer = wx.BoxSizer(wx.HORIZONTAL)
        panel1 = wx.Panel(self)
        expertHeader = wx.StaticText(panel1, wx.ID_ANY, 'NPV', style = wx.ALIGN_CENTER)
        expertHeader.SetFont(font)
        expertHeaderSizer.Add(expertHeader, 1)
        panel1.SetSizer(expertHeaderSizer)

        self.splitter = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D)

        self.p1 = wx.Panel(self.splitter)
        ctrlSizer = wx.BoxSizer(wx.HORIZONTAL)
        portfList = PortfolioCtrl(self.p1, -1, allFile)
        ctrlSizer.Add(portfList, 1, wx.EXPAND|wx.ALL, 10)
        self.p1.SetSizer(ctrlSizer)

        self.p2 = wx.Panel(self.splitter)
        buttonSizer = wx.BoxSizer(wx.VERTICAL)

        font2 = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        nameHeader = wx.StaticText(self.p2, -1, 'Alpha for beta distribution', style = wx.ALIGN_LEFT)
        nameHeader.SetFont(font2)
        self.nameInput = wx.TextCtrl(self.p2, -1, style = wx.ALIGN_RIGHT)
        self.nameInput.SetFont(font2)
        nameHeader2 = wx.StaticText(self.p2, -1, 'Beta for beta distribution', style = wx.ALIGN_LEFT)
        nameHeader2.SetFont(font2)
        self.nameInput2 = wx.TextCtrl(self.p2, -1, style = wx.ALIGN_RIGHT)
        self.nameInput2.SetFont(font2)
        nameHeader3 = wx.StaticText(self.p2, -1, 'Number of iterations', style = wx.ALIGN_LEFT)
        nameHeader3.SetFont(font2)
        self.nameInput3 = wx.TextCtrl(self.p2, -1, style = wx.ALIGN_RIGHT)
        self.nameInput3.SetFont(font2)
        nameHeader4 = wx.StaticText(self.p2, -1, 'Discount rate, %', style = wx.ALIGN_LEFT)
        nameHeader4.SetFont(font2)
        self.nameInput4 = wx.TextCtrl(self.p2, -1, style = wx.ALIGN_RIGHT)
        self.nameInput4.SetFont(font2)

        buttonSizer.Add(nameHeader, 0)
        buttonSizer.Add(self.nameInput, 0, wx.ALL|wx.EXPAND, 5)
        buttonSizer.Add(nameHeader2, 0)
        buttonSizer.Add(self.nameInput2, 0, wx.ALL|wx.EXPAND, 5)
        buttonSizer.Add(nameHeader3, 0)
        buttonSizer.Add(self.nameInput3, 0, wx.ALL|wx.EXPAND, 5)
        buttonSizer.Add(nameHeader4, 0)
        buttonSizer.Add(self.nameInput4, 0, wx.ALL|wx.EXPAND, 5)
        self.p2.SetSizer(buttonSizer)

        self.splitter.SplitVertically(self.p1, self.p2, int(self.parent.width * 0.85))

        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        panel3 = wx.Panel(self)
        ButOK = wx.Button(panel3, -1, 'To build')
        ButOK.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButOK.Bind(wx.EVT_BUTTON, lambda event: self.OnBuild(event, allFile))
        buttonSizer.Add(ButOK, 1, wx.ALL, 5)
        panel3.SetSizer(buttonSizer)

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(panel1, 0, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add((-1, 20))
        mainSizer.Add(self.splitter, 1, wx.EXPAND)
        mainSizer.Add(panel3, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        self.SetSizer(mainSizer)
        self.Layout()

    def OnBuild(self, e, allFile):
        allFile.rate = float(self.nameInput4.GetValue()) / 100.0
        allFile.numOfIterations = int(self.nameInput3.GetValue())
        allFile.alpha = int(self.nameInput.GetValue())
        allFile.betta = int(self.nameInput2.GetValue())
        allFile.buildNPV()
        self.parent.Destroy()


class NPV(wx.Frame):
    def __init__(self, allFile, *args, **kwargs):
        self.width = 800
        wx.Frame.__init__(self, wx.GetApp().TopWindow, title='Experts',
                          size = (self.width,480))
        self.InitUI(allFile)

    def InitUI(self, allFile):
        self.panel = HeadPanel(self, allFile)
        self.panel.Bind(wx.EVT_PAINT, self.Paint)
        Menus.MenuBar(self)
        self.statusBar = self.CreateStatusBar()
        self.statusBar.SetStatusText('Ready')
        self.SetTitle('ProgRa')
        self.Centre()

    def Paint(self, e):
        self.width = self.GetSize()[0]
        self.panel.splitter.SetSashPosition(int((self.width)*0.55))
        self.Update()