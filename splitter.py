# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''

import wx, sys
import Menus, ToolBar, Projects, MainPanel

class Example(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(Example, self).__init__(*args, **kwargs)
        self.InitUI()

    def InitUI(self):
        mainsizer = wx.BoxSizer(wx.VERTICAL)
        headersizer = wx.BoxSizer(wx.HORIZONTAL)
        self.panel = wx.Panel(self)
        self.curProjects = wx.StaticText(self.panel, wx.ID_ANY, 'Current Projects', style = wx.ALIGN_CENTER)
        self.choProjects = wx.StaticText(self.panel, wx.ID_ANY, 'Chosen Projects', style = wx.ALIGN_CENTER)
        headersizer.Add(self.curProjects, 1)
        headersizer.Add(self.choProjects, 1, wx.EXPAND)
        self.panel.SetSizer(headersizer)
        # self.panel.Layout()

        self.splitter = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D)

        p1 = MainPanel.MyLeftCtrl(self.splitter, -1)
        p2 = MainPanel.MyRightCtrl(self.splitter, -1)
        self.splitter.SplitVertically(p1, p2, 400)

        Menus.MenuBar(self)
        ToolBar.ToolBar(self)

        # mainsizer.Add((-1, 40))
        mainsizer.Add(headersizer, 0, wx.EXPAND)
        mainsizer.Add(self.splitter, 1, wx.EXPAND)
        self.SetSizer(mainsizer)

        self.statusBar = self.CreateStatusBar()
        self.statusBar.SetStatusText('Ready')

        self.SetSize((800, 800))
        self.SetTitle('ProgRa')
        self.Centre()
        self.Show(True)


def main():

    ex = wx.App()
    Example(None)
    ex.MainLoop()    


if __name__ == '__main__':
    main()