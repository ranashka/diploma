# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''


#To Do rename

import wx, sys
import Menus, ToolBar, Projects, MainPanel, OpenSave
from Parser import TotalFile
import os

class AllPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

        self.parent = parent
        #Focus of splitter windows
        self.LeftFocus = False
        self.RightFocus = False
        self.selectedProject = ''
        self.selectedItem = 0

        font = wx.Font(20, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        headersizer = wx.BoxSizer(wx.HORIZONTAL)
        panel1 = wx.Panel(self)
        curProjects = wx.StaticText(panel1, wx.ID_ANY, 'Current Projects', style = wx.ALIGN_CENTER)
        curProjects.SetFont(font)
        choProjects = wx.StaticText(panel1, wx.ID_ANY, 'Chosen Projects', style = wx.ALIGN_CENTER)
        choProjects.SetFont(font)
        headersizer.Add(curProjects, 1, wx.EXPAND)
        headersizer.Add(choProjects, 1, wx.EXPAND)
        panel1.SetSizer(headersizer)

        self.splitter = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D)
        self.p1 = MainPanel.MyLeftCtrl(self.splitter, -1, self.parent.allFile)
        self.p1.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnLeftFocus)
        self.p1.Bind(wx.EVT_LEFT_DCLICK, self.OnMoveLeft)
        self.p2 = MainPanel.MyRightCtrl(self.splitter, -1, self.parent.allFile)
        self.p2.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnRightFocus)
        self.p2.Bind(wx.EVT_LEFT_DCLICK, self.OnMoveRight)
        self.splitter.SplitVertically(self.p1, self.p2, int((parent.width)/2))

        panel2 = wx.Panel(self)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        button01 = wx.Button(panel2, -1, 'Add', size = (100,30))
        button01.Font = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        button01.Bind(wx.EVT_BUTTON, self.OnAdd)
        buttonSizer.Add((10, 1))
        buttonSizer.Add(button01, 1, wx.ALIGN_LEFT)
        button02 = wx.Button(panel2, -1, 'Edit', size = (100,30))
        button02.Font = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        button02.Bind(wx.EVT_BUTTON, self.OnEdit)
        buttonSizer.Add((10, 1))
        buttonSizer.Add(button02, 1, wx.ALIGN_LEFT)
        button03 = wx.Button(panel2, -1, 'Delete', size = (100,30))
        button03.Font = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        button03.Bind(wx.EVT_BUTTON, self.OnDelete)
        buttonSizer.Add((10, 1))
        buttonSizer.Add(button03, 1, wx.ALIGN_LEFT)
        button1 = wx.Button(panel2, -1, 'Move', size = (110,30))
        button1.Font = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        button1.Bind(wx.EVT_BUTTON, self.OnMove)
        buttonSizer.Add((10, 1))
        buttonSizer.Add(button1, 1, wx.ALIGN_LEFT)
        button2 = wx.Button(panel2, -1, 'Create portfolio', size = (140,30))
        button2.Font = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        button2.Bind(wx.EVT_BUTTON, self.OnCreatePortfolio)
        buttonSizer.Add((70, 1))
        buttonSizer.Add(button2, 1, wx.ALIGN_LEFT)
        buttonSizer.Add((100, 1))
        panel2.SetSizer(buttonSizer)

        panel5 = wx.Panel(self)
        portfolioSizer = wx.BoxSizer(wx.VERTICAL)
        font2 = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        portfolioHeader = wx.StaticText(panel5, -1, 'Portfolio', style = wx.ALIGN_CENTER, size = wx.Size(150, 30))
        portfolioHeader.SetFont(font2)
        self.portfolioInput = MainPanel.PortfolioCtrl(panel5, -1, totalFile=self.parent.allFile)
        portfolioSizer.Add(portfolioHeader, 0, wx.ALIGN_CENTER)
        portfolioSizer.Add(self.portfolioInput, 1, wx.EXPAND)
        panel5.SetSizer(portfolioSizer)

        panel3 = wx.Panel(self)
        font = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        header1 = wx.StaticText(panel3,wx.ID_ANY,'Available amount', style = wx.ALIGN_CENTER)
        header2 = wx.StaticText(panel3,wx.ID_ANY,'Required amount', style = wx.ALIGN_CENTER)
        header1.SetFont(font)
        header2.SetFont(font)
        headerSizer2 = wx.BoxSizer(wx.HORIZONTAL)
        headerSizer2.Add(header1, 1)
        headerSizer2.Add(header2, 1)
        panel3.SetSizer(headerSizer2)

        panel4 = wx.Panel(self)
        font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        self.availAmount = wx.TextCtrl(panel4, -1, value = '0')
        self.requireAmount = wx.TextCtrl(panel4, -1, value = '0')
        summa = self.p2.project.getChosenSum()
        self.requireAmount.SetValue(str(summa))
        self.availAmount.SetFont(font)
        self.requireAmount.SetFont(font)
        ctrlSizer = wx.BoxSizer(wx.HORIZONTAL)
        ctrlSizer.Add(self.availAmount, 1, wx.LEFT, 20)
        ctrlSizer.Add((20, 20))
        ctrlSizer.Add(self.requireAmount, 1, wx.RIGHT, 20)
        panel4.SetSizer(ctrlSizer)

        panel6 = wx.Panel(self)
        button3 = wx.Button(panel6, -1, 'Delete portfolio', size = (150,30))
        button3.Font = wx.Font(14, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        button3.Bind(wx.EVT_BUTTON, self.OnDeletePortfolio)
        deleteButSizer = wx.BoxSizer(wx.HORIZONTAL)
        deleteButSizer.Add(button3, 1, wx.ALIGN_CENTER_HORIZONTAL)
        panel6.SetSizer(deleteButSizer)

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(panel1, 0, flag = wx.ALL|wx.EXPAND)
        self.sizer.Add(self.splitter,0, wx.EXPAND)
        self.sizer.Add(panel2, 0, wx.ALL|wx.ALIGN_LEFT, 10)
        self.sizer.Add(panel3, 0, wx.EXPAND)
        self.sizer.Add(panel4, 0, wx.EXPAND)
        self.sizer.Add(panel5, 0, wx.EXPAND|wx.ALL, 5)
        self.sizer.Add(panel6, 1, wx.ALL|wx.ALIGN_CENTER, 5)
        self.SetSizer(self.sizer)
        self.Layout()

    def OnAdd(self, e):
        Projects.ProjectInfoDialog(self.parent.allFile, self).Show()

    def OnEdit(self, e):
        Projects.ProjectInfoDialog(self.parent.allFile, self).Show()

    def OnDelete(self, e):
        if self.RightFocus:
            self.parent.allFile.delProjectAll(self.selectedProject)
            self.p2.DeleteItem(self.selectedItem)
        else:
            self.parent.allFile.delProjectAll(self.selectedProject)
            self.p1.DeleteItem(self.selectedItem)

    def OnMoveLeft(self, e):
        item = self.p1.GetFocusedItem()
        index = self.p2.InsertStringItem(sys.maxint, self.p1.GetItemText(item,0))
        self.p2.SetStringItem(index, 1, self.p1.GetItemText(item,1))
        self.p2.SetStringItem(index, 2, self.p1.GetItemText(item,2))
        self.p2.SetStringItem(index, 3, self.p1.GetItemText(item,3))
        itemTuple = (self.p1.GetItemText(item,0), self.p1.GetItemText(item,1), self.p1.GetItemText(item,2), self.p1.GetItemText(item,3))
        self.p2.addChoice(itemTuple)
        self.p1.deleteUnchosen(self.selectedProject)
        self.p1.DeleteItem(item)
        self.UpdateSum(e)

    def OnLeftFocus(self, e):
        self.LeftFocus = True
        self.RightFocus = False
        self.selectedProject = self.p1.GetItemText(self.p1.GetFocusedItem(), 0)
        self.selectedItem = self.p1.GetFocusedItem()

    def OnMoveRight(self, e):
        item = self.p2.GetFocusedItem()
        index = self.p1.InsertStringItem(sys.maxint, self.p2.GetItemText(item,0))
        self.p1.SetStringItem(index, 1, self.p2.GetItemText(item,1))
        self.p1.SetStringItem(index, 2, self.p2.GetItemText(item,2))
        self.p1.SetStringItem(index, 3, self.p2.GetItemText(item,3))
        itemTuple = (self.p2.GetItemText(item,0), self.p2.GetItemText(item,1), self.p2.GetItemText(item,2), self.p2.GetItemText(item,3))
        self.p1.addUnchoice(itemTuple)
        self.p2.deleteChoice(self.selectedProject)
        self.p2.DeleteItem(item)
        self.UpdateSum(e)

    def OnRightFocus(self, e):
        self.RightFocus = True
        self.LeftFocus = False
        self.selectedProject = self.p2.GetItemText(self.p2.GetFocusedItem(), 0)
        self.selectedItem = self.p2.GetFocusedItem()

    def OnMove(self, e):
        if self.LeftFocus and self.p1.GetFocusedItem() is not None:
            self.OnMoveLeft(e)
            self.LeftFocus = False
        elif self.RightFocus and  self.p2.GetFocusedItem() is not None:
            self.OnMoveRight(e)
            self.RightFocus = False
        else:
            wx.MessageBox('You haven`t chosen a project', 'Info',
                          wx.OK | wx.ICON_INFORMATION)

    def UpdateSum(self, e):
        summa = self.p2.project.getChosenSum()
        self.requireAmount.SetValue(str(summa))

    def OnCreatePortfolio(self, e):
        self.p1.updateCtrl()
        self.p2.updateCtrl()
        addingString = self.parent.allFile.createPortfolio()
        self.portfolioInput.addPortfolio(addingString)
        self.requireAmount.SetValue('0')

    def OnDeletePortfolio(self, e):
        item = self.portfolioInput.GetFocusedItem()
        self.portfolioInput.DeleteItem(item)
        self.parent.allFile.deletePortfolio(item)

    def delValue(self, e):
        self.requireAmount.Remove()

class Example(wx.Frame):
    def __init__(self, inFile = 'docs\Temp.tmp', *args, **kwargs):
        self.width = 800
        self.inFile = inFile
        wx.Frame.__init__(self, None, title="Nested Splitters",
                           size=(self.width,650))
        self.InitUI(self.inFile)

    def InitUI(self, inFile):
        self.inFile = inFile
        self.allFile = TotalFile(inFile)
        self.allFile.openFile()
        self.panel = AllPanel(self)
        Menus.MenuBar(self)
        ToolBar.ToolBar(self)
        self.statusBar = self.CreateStatusBar()
        self.statusBar.SetStatusText('Ready')
        self.panel.Bind(wx.EVT_PAINT, self.Paint)
        self.SetTitle('ProgRa')
        self.Centre()
        self.Bind(wx.EVT_CLOSE, self.OnClose)

    def InitUINew(self, inFile):
        self.Hide()
        self.Update(inFile = 'docs\All.rna')
        self.Show()

    def Paint(self, e):
        self.width = self.GetSize()[0]
        self.panel.splitter.SetSashPosition(int((self.width)/2))
        self.Update()

    def OnClose(self, e):
        path = OpenSave.OnSaveAs(self, e)
        self.allFile.writeFile(path)
        self.allFile.delTempFiles(os.path.dirname(path))
        self.Destroy()

    def OnNew(self, e):
        self.allFile.writeFile()
        newWindow = Example('docs\Temp.tmp')
        newWindow.Show()
        self.Destroy()

    def OnOpen(self, path):
        # self.allFile.writeFile()
        newWindow = Example(path)
        newWindow.Show()
        self.Destroy()

    def OnExit(self, e):
        self.Destroy()

def main():
    ex = wx.App()
    frame = Example()
    frame.Show()
    ex.MainLoop()

if __name__ == '__main__':
    main()