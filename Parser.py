# -*- coding: utf-8 -*-
import codecs
import OpenSave
import shutil
import os
import numpy
import matplotlib.pyplot as plt
import scipy
from scipy.interpolate import spline

class TotalFile:
    def __init__(self, inFile):
        self.inFile = inFile
        self.rate = 0.25
        self.numOfIterations = 1000
        self.alpha = 2
        self.betta = 3
        self.chosenProjects = []
        self.unchosenProjects = []
        self.experts = []
        self.expertsToAdd = []
        self.evoluations = []
        self.evoluationsToAdd = []
        self.updatedEvoluations = []
        self.expertsToDel = []
        self.evoluationsToDel = []
        self.risks = []
        self.updatedRisks = []
        self.portfolios = []
        self.expertRate = {}
        self.optimisticEval = {}
        self.realisticEval = {}
        self.pessimisticEval = {}
        self.imitations = {}
        self.fileNames = {'CH': self.chosenProjects,
                          'EE': self.evoluations,
                          'EX': self.experts,
                          'PR': self.unchosenProjects,
                          'RR': self.risks,
                          'PORTFOLIO': self.portfolios}

        self.finalProjectEvaluations = {}
        self.finalProjectEvalRisks = {}


    def openFile(self):
        try:
            f = codecs.open (self.inFile, 'r', encoding='utf-8')
            for line in f:
                key = line.split('\\')[0]
                if key in self.fileNames.keys():
                    self.fileNames[key].append(line.split('\\')[1:])
            f.close()
            self.expertRates()
            self.divideEvol()
        except:
            f = codecs.open (self.inFile, 'w', encoding='utf-8')
            print "Couldn`t open the file %s" %str(self.inFile)
            f.close()

    def writeFile(self, outFile):
        # try:
            self.fileNames = {'CH': self.chosenProjects,
                              'EE': self.evoluations,
                              'EX': self.experts,
                              'PR': self.unchosenProjects,
                              'RR': self.risks,
                              'PORTFOLIO': self.portfolios}
            f = codecs.open (outFile, 'w', encoding='utf-8')
            for key in self.fileNames.keys():
                for line in self.fileNames[key]:
                    string = str(key)
                    for item in line:
                        string += '\\' + unicode(item)
                    f.write(string)
            f.close()

        # except:
        #     print "Couldn`t open the file %s" %str(outFile)

    def expertRates(self):
        totSum = 0
        for expert in self.experts:
            totSum += int(expert[4])
        for expert in self.experts:
            self.expertRate[expert[0]] = float(expert[4])/totSum

    def divideEvol(self):
        for evaluate in self.evoluations:
            rate = self.expertRate[evaluate[0]]
            if evaluate[2] == 'O':
                if not self.optimisticEval.has_key(evaluate[1]):
                    self.optimisticEval[evaluate[1]] = []
                self.optimisticEval[evaluate[1]].append(rate)
                nums = evaluate[3:]
                for num in nums:
                    self.optimisticEval[evaluate[1]].append(num.strip())

            elif evaluate[2] == 'R':
                if not self.realisticEval.has_key(evaluate[1]):
                    self.realisticEval[evaluate[1]] = []
                self.realisticEval[evaluate[1]].append(rate)
                nums = evaluate[3:]
                for num in nums:
                    self.realisticEval[evaluate[1]].append(num.strip())

            elif evaluate[2] == 'P':
                if not self.pessimisticEval.has_key(evaluate[1]):
                    self.pessimisticEval[evaluate[1]] = []
                self.pessimisticEval[evaluate[1]].append(rate)
                nums = evaluate[3:]
                for num in nums:
                    self.pessimisticEval[evaluate[1]].append(num.strip())
            else:
                print "Wrong format of the file"

    def delTempFiles(self, path):
        for f in os.listdir(path):
            if f.endswith(".tmp"):
                os.remove(path + '\\' + f)

    def createPortfolio(self):
        next_num = str(len(self.portfolios) + 1)
        adding = ''
        summa = 0.0
        for chosen in self.chosenProjects:
            adding += chosen[0] + ','
            summa += float(chosen[3])
        adding = adding[:-1]
        self.portfolios.append([next_num, adding, str(int(summa))+'\r\n'])

        for project in self.chosenProjects:
            self.unchosenProjects.append(project)

        lenth = len(self.chosenProjects)
        for i in range(lenth):
            del self.chosenProjects[0]

        return [next_num, adding, str(int(summa))+'\r\n']

    def deletePortfolio(self, item):
        del self.portfolios[item]

    def getExprertInfo(self, expertID):
        expertDict = []
        for item in self.evoluations:
            expertLine = []
            if str(item[0]) == str(expertID):
                expertLine.append(self.getNameOfProject(item[1]))
                for it in item[2:]:
                    expertLine.append(it)
                expertDict.append(expertLine)
        for item in self.evoluationsToAdd:
            expertLine = []
            if str(item[0]) == str(expertID):
                expertLine.append(self.getNameOfProject(item[1]))
                for it in item[2:]:
                    expertLine.append(it)
                expertDict.append(expertLine)
        return expertDict

    def getNameOfProject(self, projectID):
        for project in self.chosenProjects:
            if str(project[0]) == str(projectID):
                return project[1]
        for project in self.unchosenProjects:
            if str(project[0]) == str(projectID):
                return project[1]
        return ''

    def IDOfProject(self, projectName):
        for project in self.chosenProjects:
            if unicode(project[1]) == unicode(projectName):
                return project[0]
        for project in self.unchosenProjects:
            if unicode(project[1]) == unicode(projectName):
                return project[0]
        return ''

    def ProjectInfo(self, projectID):
        projectInfo=[]
        for project in self.chosenProjects:
            if str(project[0]) == str(projectID):
                for item in project:
                    projectInfo.append(item.strip())
        for project in self.unchosenProjects:
            if str(project[0]) == str(projectID):
                for item in project:
                    projectInfo.append(item.strip())
        return projectInfo

    def DelProjectForChange(self, projectID):
        counter = 0
        for project in self.chosenProjects:
            counter +=1
            if str(project[0]) == str(projectID):
                del self.chosenProjects[0]
        counter = 0
        for project in self.unchosenProjects:
            if str(project[0]) == str(projectID):
                del self.unchosenProjects[0]

    def getExpertFIO(self, expertID, FIOonly = False):
        for item in self.experts:
            if str(item[0]) == str(expertID):
                if FIOonly:
                    string = item[0]
                    for i in item[1:-1]:
                        string += ' '+ i
                    return string
                else:
                    return item[1:]
        for item in self.expertsToAdd:
            if str(item[0]) == str(expertID):
                if FIOonly:
                    string = item[0]
                    for i in item[1:-1]:
                        string += ' '+ i
                    return string
                else:
                    return item[1:]
        return ''


    def delExpert(self, expertID):
        counter = 0
        for item in self.experts:
            if str(item[0]) == str(expertID):
                del self.experts[counter]
                break
            counter += 1
        counter = 0
        for item in self.expertsToAdd:
            if str(item[0]) == str(expertID):
                del self.expertsToAdd[counter]
                break
            counter += 1

        tempDict = []
        for evol in self.evoluations:
            if str(evol[0]) != str(expertID):
                tempDict.append(evol)
        del self.evoluations
        self.evoluations = tempDict
        tempDict2 = []
        for evol in self.evoluationsToAdd:
            if str(evol[0]) != str(expertID):
                tempDict2.append(evol)
        del self.evoluationsToAdd
        self.evoluationsToAdd = tempDict2

    def delOneEvol(self, evol):
        counter = 0
        for evoluation in self.evoluations:
            if evol[0] == evoluation[0] and\
                evol[1] == evoluation[1] and\
                evol[2] == evoluation[2]:
                del self.evoluations[counter]
        counter = 0
        for evoluation in self.evoluationsToAdd:
            if evol[0] == evoluation[0] and \
                evol[1] == evoluation[1] and \
                evol[2] == evoluation[2]:
                del self.evoluationsToAdd[counter]

    def delProjectAll(self, item):
        counter = 0
        for i in self.chosenProjects:
            if i[0] == item:
                del self.chosenProjects[counter]
            counter +=1
        counter = 0
        for i in self.unchosenProjects:
            if i[0] == item:
                del self.unchosenProjects[counter]
            counter +=1
        tempDict = []
        counter = 0
        for i in self.evoluations:
            if i[1] != item:
                tempDict.append(self.evoluations[counter])
            counter +=1
        del self.evoluations
        self.evoluations = tempDict
        tempDict2 = []
        counter = 0
        for i in self.risks:
            if i[1] != item:
                tempDict2.append(self.risks[counter])
            counter +=1
        del self.risks
        self.risks = tempDict2

    def getExpertLine(self):
        listOfExperts = []
        for expert in self.experts:
            string = expert[0]
            for item in expert[1:-1]:
                string += (' ' + item)
            listOfExperts.append(string)
        return listOfExperts

    def getRiskInfo(self):
        riskInfo=[]
        for risk in self.risks:
            lineToAdd = []
            lineToAdd.append(self.getExpertFIO(risk[0],True))
            lineToAdd.append(self.getNameOfProject(risk[1]))
            lineToAdd.append(risk[2])
            lineToAdd.append(risk[3])
            lineToAdd.append(risk[4])
            riskInfo.append(lineToAdd)
        return riskInfo

    def correctRate(self, withRisks = True):
        for expert in self.experts:
            rate = self.expertRate[expert[0]]
            count=0
            for evol in self.evoluations:
                if expert[0] == evol[0]:
                    i=3
                    while i < len(evol):
                        minim, maxim = evol[i].split('-')
                        minim = int(int(minim)*rate)
                        maxim = int(int(maxim)*rate)
                        self.evoluations[count][i] = (str(minim) + '-' + str(maxim))
                        i += 1
                count += 1

            count = 0
            for risk in self.risks:
                if expert[0] == risk[0]:
                    i = 2
                    while i < len(risk):
                        self.risks[count][i] = int(int(self.risks[count][i]) * rate)
                        i += 1
                count += 1

        projectWithoutExperts = {}
        for evol in self.evoluations:
            if (str(evol[1])+str(evol[2])) not in projectWithoutExperts.keys():
                projectWithoutExperts[str(evol[1])+str(evol[2])] = []
                i = 3
                while i < len(evol):
                    minim, maxim = evol[i].split('-')
                    projectWithoutExperts[str(evol[1])+str(evol[2])] += [int(minim), int(maxim)]
                    i+=1
            else:
                i = 3
                counter = 0
                while i < len(evol):
                    minim, maxim = evol[i].split('-')
                    projectWithoutExperts[str(evol[1])+str(evol[2])][counter] += int(minim)
                    projectWithoutExperts[str(evol[1])+str(evol[2])][counter+1] += int(maxim)
                    i+=1
                    counter += 2

        risksWithoutExperts = {}
        for risk in self.risks:
            if risk[1] not in risksWithoutExperts.keys():
                i=2
                risksWithoutExperts[risk[1]] = []
                while i < 5:
                    risksWithoutExperts[risk[1]].append(int(risk[i]))
                    i+=1
            else:
                i=2
                count=0
                while i < 5:
                    risksWithoutExperts[risk[1]][count] += int(risk[i])
                    i+=1
                    count+=1

        for evol in projectWithoutExperts.keys():
            if evol[:-1] not in self.finalProjectEvaluations.keys():
                project = self.ProjectInfo(evol[:-1])
                self.finalProjectEvaluations[evol[:-1]] = []
                self.finalProjectEvaluations[evol[:-1]].append(project[1])
                self.finalProjectEvaluations[evol[:-1]].append(project[3])
                self.finalProjectEvaluations[evol[:-1]].append(project[2])
            i = 0
            counter = 3
            while i < len(projectWithoutExperts[evol]):
                if evol[-1:] in ('O', 'P'):
                    koef = 1
                elif evol[-1:] == 'R':
                    koef = 3

                if counter < len(self.finalProjectEvaluations[evol[:-1]]):
                    self.finalProjectEvaluations[evol[:-1]][counter] += projectWithoutExperts[evol][i]*koef
                else:
                    self.finalProjectEvaluations[evol[:-1]].append(projectWithoutExperts[evol][i]*koef)
                if (counter + 1) < len(self.finalProjectEvaluations[evol[:-1]]):
                    self.finalProjectEvaluations[evol[:-1]][counter + 1] += projectWithoutExperts[evol][i+1]*koef
                else:
                    self.finalProjectEvaluations[evol[:-1]].append(projectWithoutExperts[evol][i+1]*koef)
                counter += 2
                i += 2

        if withRisks == True:
            for risk in risksWithoutExperts.keys():
                self.finalProjectEvalRisks[risk] = []
                self.finalProjectEvalRisks[risk].append(risk)
                result = int((int(risksWithoutExperts[risk][0])+ int (risksWithoutExperts[risk][2]) + 3*(risksWithoutExperts[risk][1]))/6)
                self.finalProjectEvalRisks[risk].append(result)

        for project in self.finalProjectEvaluations.keys():
            i=3
            while i < len(self.finalProjectEvaluations[project]):
                self.finalProjectEvaluations[project][i] = self.finalProjectEvaluations[project][i]/6
                i += 1
        pass

    def getRandomNPVFromProject(self, num):
        for project in self.finalProjectEvaluations.keys():
            if project == num:
                investments = self.finalProjectEvaluations[num][1]
                years = self.finalProjectEvaluations[num][2]
                count = 3
                alpha = self.alpha
                beta = self.betta
                discount = self.rate
                npv = -int(investments)
                year = 1
                while count < 2 + (2 * int(years)):
                    minim = self.finalProjectEvaluations[num][count]
                    maxim = self.finalProjectEvaluations[num][count+1]
                    asser = minim + numpy.random.beta(alpha, beta)*(maxim-minim)
                    npv += asser* (1/(1+discount)**year)
                    year += 1
                    count += 2
                return npv

    def getRandomRisk(self, num):
        for project in self.finalProjectEvalRisks.keys():
            if project == num:
                if numpy.random.rand() > 0.5:
                    return self.finalProjectEvalRisks[project][1]
                else:
                    return 0

    def calculateNPV(self):
        iterations = self.numOfIterations
        for i in range(iterations):
            for portfolio in self.portfolios:
                projects = portfolio[1].split(',')
                sumNPV = 0
                for project in projects:
                    sumNPV -= self.getRandomRisk(project)
                    sumNPV += self.getRandomNPVFromProject(project)
                if portfolio[0] not in self.imitations.keys():
                    self.imitations[portfolio[0]] = []
                self.imitations[portfolio[0]].append(sumNPV)

    def getFrequency(self, portfolio):
        minim = min(portfolio)
        maxim = max(portfolio)
        pocket = minim
        pockets = [0,]
        frequencies = [0,]
        while pocket <= maxim:
            pockets.append(pocket)
            pocket += 100
        pockets.append(pocket)
        counterOfPockets = 0
        for pocket in pockets:
            counterOfFrequency = 0
            counterOfPockets += 1
            if counterOfPockets == 1:
                continue
            else:
                for iter in portfolio:
                    if iter >= (pocket-100) and iter < pocket:
                        counterOfFrequency +=1
            frequencies.append(float(counterOfFrequency)/1000.0)

        count = 0
        for freq in frequencies:
            if count == 0:
                count += 1
                continue
            else:
                frequencies[count] += frequencies[count-1]
                count += 1
        for count in range(len(frequencies)):
            frequencies[count] = 1- frequencies[count]
        return (pockets, frequencies)


    def buildNPV(self):
        self.expertRates()
        self.correctRate()
        self.calculateNPV()
        self.NPV = {}
        xMax = 0
        for portfolio in self.imitations.keys():
            self.NPV[portfolio] = self.getFrequency(self.imitations[portfolio])
            plt.plot(self.NPV[portfolio][0], self.NPV[portfolio][1], label = 'Portfolio' + portfolio)
            if self.NPV[portfolio][0][-1] > xMax:
                xMax = self.NPV[portfolio][0][-1]
        plt.axis([10000,xMax, 0, 1])
        plt.legend()
        plt.title('NPV')
        plt.show()

    def getIRR(self, projects):
        NPVlist = []
        cashFlowByYear = [0,0,0,0,0]
        investments = 0
        for project in projects:
            for pr in self.finalProjectEvaluations.keys():
                if project == pr:
                    investments += int(self.finalProjectEvaluations[pr][1])
        for n in range(5):
            for project in projects:
                for pr in self.finalProjectEvaluations.keys():
                    if project == pr:
                        count = 1 + 2*(n+1)
                        cashFlowByYear[n] += (self.finalProjectEvaluations[pr][count] + self.finalProjectEvaluations[pr][count+1])/2.0
                        break
        i= 50.0000
        while i < 150.0000:
            npv = -investments
            year = 0
            discount = i / 100.0
            for cash in cashFlowByYear:
                npv += cash / ((1 + discount)**year)
                year += 1
            NPVlist.append(npv)
            i += 1
        return NPVlist



    # def getIRR(self, projects):
    #     NPVlist = []
    #     i= 38.0000
    #     count2 = 0
    #     while i < 48.0000:
    #         totalNPV = 0
    #         for project in projects:
    #             for pr in self.finalProjectEvaluations.keys():
    #                 if project == pr:
    #                     investments = self.finalProjectEvaluations[pr][1]
    #                     years = self.finalProjectEvaluations[pr][2]
    #                     count = 3
    #                     discount = i / 100.0
    #                     npv = -int(investments)
    #                     year = 1
    #                     while count < 2 + (2 * int(years)):
    #                         minim = self.finalProjectEvaluations[pr][count]
    #                         maxim = self.finalProjectEvaluations[pr][count+1]
    #                         asser = minim + numpy.random.beta(self.alpha, self.betta)*(maxim-minim)
    #                         npv += asser * (1/(1+discount)**year)
    #                         year += 1
    #                         count += 2
    #                     totalNPV += npv
    #         if count2 > 0 and NPVlist[count2-1] < totalNPV:
    #             NPVlist.append(NPVlist[count2-1])
    #         else:
    #             NPVlist.append(totalNPV)
    #         i += 0.005
    #         count2 += 1
    #     return NPVlist

    def buildIRR(self):
        self.expertRates()
        self.correctRate()
        self.IRRImitations = {}
        for portfolio in self.portfolios:
            projects = portfolio[1].split(',')
            self.IRRImitations[portfolio[0]] = self.getIRR(projects)
            ran = []
            a=50.0000
            while a < 150.0000:
                ran.append(a)
                a += 1
            power = numpy.array(self.IRRImitations[portfolio[0]])
            xnew = numpy.linspace(min(ran),max(ran))

            power_smooth = spline(ran,power,xnew)
            line, = plt.plot(xnew, power_smooth, label = 'Portfolio' + portfolio[0])
            line.set_antialiased(False)

        plt.plot(range(50, 150),[i*0 for i in range(50, 150)])
        # plt.axis([10000,xMax, 0, 1])
        plt.legend()
        plt.title('IRR')
        plt.xlabel('Discount rate')
        plt.ylabel('NPV')
        plt.show()

    def getPP(self, projects):
        PPlist = []
        count = 3
        for year in range(1,6):
            totalNPV = 0
            for project in projects:
                for pr in self.finalProjectEvaluations.keys():
                    if project == pr:
                        discount = self.rate
                        if year == 1:
                            investments = self.finalProjectEvaluations[pr][1]
                            npv = -int(investments)
                        minim = self.finalProjectEvaluations[pr][count]
                        maxim = self.finalProjectEvaluations[pr][count+1]
                        asser = minim + numpy.random.beta(self.alpha, self.betta)*(maxim-minim)
                        npv += asser * (1/(1+discount)**(year))
                        totalNPV += npv
            PPlist.append(totalNPV)
            count += 2
        return PPlist

    def buildPP(self):
        self.expertRates()
        self.correctRate()
        self.PPImitations = {}
        for portfolio in self.portfolios:
            projects = portfolio[1].split(',')
            self.PPImitations[portfolio[0]] = self.getPP(projects)
            plt.plot(range(1, 6), self.PPImitations[portfolio[0]], label = 'Portfolio' + portfolio[0])
        plt.plot(range(1, 6),[i*0 for i in range(1, 6)])
        plt.legend()
        plt.title('PP')
        plt.xlabel('Years')
        plt.ylabel('NPV')
        plt.show()

    def calculatePI(self):
        self.PIlist = {}
        iterations = self.numOfIterations
        for i in range(iterations):
            for portfolio in self.portfolios:
                projects = portfolio[1].split(',')
                sumPortfolio = float(portfolio[2])
                sumNPV = 0
                for project in projects:
                    sumNPV -= self.getRandomRisk(project)
                    sumNPV += self.getRandomPIFromProject(project)
                if portfolio[0] not in self.PIlist.keys():
                    self.PIlist[portfolio[0]] = []
                self.PIlist[portfolio[0]].append(float(sumNPV)/sumPortfolio)
        return self.PIlist

    def getRandomPIFromProject(self, num):
        for project in self.finalProjectEvaluations.keys():
            if project == num:
                years = self.finalProjectEvaluations[num][2]
                count = 3
                alpha = self.alpha
                beta = self.betta
                discount = self.rate
                npv = 0
                year = 1
                while count < 2 + (2 * int(years)):
                    minim = self.finalProjectEvaluations[num][count]
                    maxim = self.finalProjectEvaluations[num][count+1]
                    asser = minim + numpy.random.beta(alpha, beta)*(maxim-minim)
                    npv += asser* (1/(1+discount)**year)
                    year += 1
                    count += 2
                return npv

    def getPIFrequency(self, portfolio):
        minim = min(portfolio)
        maxim = max(portfolio)
        pocket = float(minim)
        pockets = []
        frequencies = [0,]
        while pocket <= maxim:
            pockets.append(pocket)
            pocket += 0.02
        pockets.append(pocket)
        counterOfPockets = 0
        for pocket in pockets:
            counterOfFrequency = 0
            counterOfPockets += 1
            if counterOfPockets == 1:
                continue
            else:
                for iter in portfolio:
                    if iter >= (pocket-0.02) and iter < pocket:
                        counterOfFrequency +=1
            frequencies.append(float(counterOfFrequency)/1000.0)

        for count  in range(len(frequencies)):
            if count == 0:
                continue
            else:
                frequencies[count] += frequencies[count-1]
        for count in range(len(frequencies)):
            frequencies[count] = 1- frequencies[count]
        return (pockets, frequencies)

    def buildPI(self):
        self.expertRates()
        self.correctRate()
        PIlist = self.calculatePI()
        self.PI = {}
        xMax = 0
        for portfolio in PIlist.keys():
            self.PI[portfolio] = self.getPIFrequency(PIlist[portfolio])
            plt.plot(self.PI[portfolio][0], self.PI[portfolio][1], label = 'Portfolio' + portfolio)
            if self.PI[portfolio][0][-1] > xMax:
                xMax = self.PI[portfolio][0][-1]
        plt.legend()
        plt.title('PI')
        plt.xlabel('PI')
        plt.ylabel('Probability')
        plt.show()
