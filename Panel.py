import wx

class MainPanel():
    def __init__(self, parent):
        self.parent = parent

        self.parent.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.parent.panel = wx.Panel(self.parent, wx.ID_ANY)

        lbls = ["labelOne", "lblTwo", "lblThree"]
        for lbl in lbls:
            self.buildLayout(lbl)
        self.parent.panel.SetSizer(self.parent.mainSizer)

    #----------------------------------------------------------------------
    def buildLayout(self, text):
        """"""
        lblSize = (60,-1)
        lbl = wx.StaticText(self.parent.panel, label=text, size=lblSize)
        txt = wx.TextCtrl(self.parent.panel)

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(lbl, 0, wx.ALL|wx.ALIGN_LEFT, 5)
        sizer.Add(txt, 0, wx.ALL, 5)
        self.parent.mainSizer.Add(sizer)

