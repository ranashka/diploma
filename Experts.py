# -*- coding: utf-8 -*-

import wx, sys
import Menus, ToolBar, Projects, MainPanel
from Expert_h import Expert
from ExpertInfo import ExpertInfoDialog

class LeftCtrl(wx.ListCtrl):
        def __init__(self, parent, id, allFile):
            wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT)

            self.parent = parent
            self.experts = Expert()

            self.listOfExperts = allFile.experts
            self.InsertColumn(0, 'ID', width = 30)
            self.InsertColumn(1, 'Name', width=150)
            self.InsertColumn(2, 'Surname', width=150)
            self.InsertColumn(3, 'Midname', width=150)
            self.InsertColumn(4, 'Competence level', width=150)

            for i in self.listOfExperts:
                index = self.InsertStringItem(sys.maxint, i[0])
                self.SetStringItem(index, 1, i[1])
                self.SetStringItem(index, 2, i[2])
                self.SetStringItem(index, 3, i[3])
                self.SetStringItem(index, 4, i[4].strip())


class HeadPanel(wx.Panel):
    def __init__(self, parent, allFile):
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        self.parent = parent
        self.chosenExpert = ''
        self.item = 0

        font = wx.Font(20, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)

        expertHeaderSizer = wx.BoxSizer(wx.HORIZONTAL)
        panel1 = wx.Panel(self)
        expertHeader = wx.StaticText(panel1, wx.ID_ANY, 'Experts information', style = wx.ALIGN_CENTER)
        expertHeader.SetFont(font)
        expertHeaderSizer.Add(expertHeader, 1)
        panel1.SetSizer(expertHeaderSizer)

        self.splitter = wx.SplitterWindow(self, wx.ID_ANY, style=wx.SP_3D)

        ctrlPanel = wx.Panel(self.splitter)
        ctrlSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.p1 = LeftCtrl(ctrlPanel, -1, allFile)
        self.p1.Bind(wx.EVT_LIST_ITEM_SELECTED, lambda event: self.OnSelect(event, allFile))
        self.p1.Bind(wx.EVT_LEFT_DCLICK, lambda event: self.OnEdit(event, allFile))
        ctrlSizer.Add(self.p1, 1, wx.ALL|wx.EXPAND, 10)
        ctrlPanel.SetSizer(ctrlSizer)

        self.p2 = wx.Panel(self.splitter)
        buttonSizer = wx.BoxSizer(wx.VERTICAL)
        ButAdd = wx.Button(self.p2, -1, 'Add')
        ButEdit = wx.Button(self.p2, -1, 'Edit')
        ButDel = wx.Button(self.p2, -1, 'Delete')
        ButAdd.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButEdit.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButDel.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButAdd.Bind(wx.EVT_BUTTON, lambda event: self.OnAdd(event, allFile))
        ButEdit.Bind(wx.EVT_BUTTON, lambda event: self.OnEdit(event, allFile))
        ButDel.Bind(wx.EVT_BUTTON, lambda event: self.OnDel(event, allFile))
        buttonSizer.Add(ButAdd, 0, wx.ALL|wx.ALIGN_CENTER, 10)
        buttonSizer.Add(ButEdit, 0, wx.ALL|wx.ALIGN_CENTER, 10)
        buttonSizer.Add(ButDel, 0, wx.ALL|wx.ALIGN_CENTER, 10)
        self.p2.SetSizer(buttonSizer)
        self.splitter.SplitVertically(ctrlPanel, self.p2, int(self.parent.width * 0.85))

        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        panel3 = wx.Panel(self)
        ButOK = wx.Button(panel3, -1, 'OK')
        ButApply = wx.Button(panel3, -1, 'Apply')
        ButCancel = wx.Button(panel3, -1, 'Cancel')
        ButOK.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButApply.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButCancel.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButOK.Bind(wx.EVT_BUTTON, lambda event: self.OnOK(event, allFile))
        ButApply.Bind(wx.EVT_BUTTON, lambda event: self.OnApply(event, allFile))
        ButCancel.Bind(wx.EVT_BUTTON, self.OnCancel)
        buttonSizer.Add(ButOK, 1, wx.ALL, 5)
        buttonSizer.Add(ButApply, 1, wx.ALL, 5)
        buttonSizer.Add(ButCancel, 1, wx.ALL, 5)
        panel3.SetSizer(buttonSizer)

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        mainSizer.Add(panel1, 0, wx.ALL|wx.EXPAND, 5)
        mainSizer.Add((-1, 20))
        mainSizer.Add(self.splitter, 1, wx.EXPAND)
        mainSizer.Add(panel3, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        self.SetSizer(mainSizer)
        self.Layout()

    def OpenAddInfo(self, e):
       pass

    def OnAdd(self, e, allFile):
        self.expertDialog = ExpertInfoDialog(allFile, self)
        self.expertDialog.Show()

    def OnSelect(self, e, allFile):
        self.item = self.p1.GetFocusedItem()
        self.chosenExpert = self.p1.GetItemText(self.item,0)

    def OnEdit(self, e, allFile):
        self.expertDialog = ExpertInfoDialog(allFile, self, self.chosenExpert, self.item)
        self.expertDialog.Show()

    def OnDel(self, e, allFile):
        self.p1.DeleteItem(self.item)
        allFile.expertsToDel.append(self.chosenExpert)

    def OnOK(self, e, allFile):
        self.OnApply(e, allFile)
        self.parent.Destroy()

    def OnApply(self, e, allFile):
        for expert in allFile.expertsToDel:
            allFile.delExpert(expert)
        for info in allFile.evoluationsToDel:
            allFile.delOneEvol(info)
        for expert in allFile.expertsToAdd:
            allFile.experts.append(expert)
        for info in allFile.evoluationsToAdd:
            allFile.evoluations.append(info)

        allFile.expertsToDel = []
        allFile.evoluationsToDel = []
        allFile.expertsToAdd = []
        allFile.evoluationsToAdd = []


    def OnCancel(self, e):
        self.parent.Destroy()


class Experts(wx.Frame):
    def __init__(self, allFile, panel, *args, **kwargs):
        self.width = 800
        self.allFile = allFile
        wx.Frame.__init__(self, wx.GetApp().TopWindow, title='Experts',
                          size = (self.width,480))

        self.InitUI(allFile, panel)

    def InitUI(self, allFile, panel):
        self.panel = HeadPanel(self, allFile)
        self.panel.Bind(wx.EVT_PAINT, self.Paint)
        Menus.MenuBar(self)
        self.statusBar = self.CreateStatusBar()
        self.statusBar.SetStatusText('Ready')
        self.SetTitle('ProgRa')
        self.Centre()

    def Paint(self, e):
        self.width = self.GetSize()[0]
        self.panel.splitter.SetSashPosition(int((self.width)*0.85))
        self.Update()

def OnClose(self, e):
    self.Destroy()