# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''

import wx
import Menus, ToolBar, Projects, MainPanel

class Example(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(Example, self).__init__(*args, **kwargs)
        self.panel = wx.Panel(self,-1)
        self.InitUI()

    def InitUI(self):
        Menus.MenuBar(self)
        ToolBar.ToolBar(self)
        MainPanel.MainPanel(self)

        self.statusBar = self.CreateStatusBar()
        self.statusBar.SetStatusText('Ready')

        self.SetSize((650, 650))
        self.SetTitle('ProgRa')
        self.Centre()
        self.Show(True)


def main():

    ex = wx.App()
    Example(None)
    ex.MainLoop()    


if __name__ == '__main__':
    main()