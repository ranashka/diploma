# -*- coding: utf-8 -*-
import wx, sys
from Expert_h import Expert

class RisksDialog(wx.Frame):
    def __init__(self, allFile, *args, **kwargs):
        self.width = 1000
        self.expertID = ''
        self.itemToDel = ''
        wx.Frame.__init__(self, wx.GetApp().TopWindow, title='ProjectRisks',
                          size = (self.width,450))

        self.InitUI(allFile)


    def InitUI(self, allFile):

        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        panel1 = wx.Panel(pnl)
        headerSizer = wx.BoxSizer(wx.HORIZONTAL)
        font = wx.Font(20, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        expertHeader = wx.StaticText(panel1, wx.ID_ANY, 'Add risk information', style = wx.ALIGN_CENTER)
        expertHeader.SetFont(font)
        headerSizer.Add(expertHeader, 1, wx.EXPAND)
        panel1.SetSizer(headerSizer)

        self.splitter = wx.SplitterWindow(pnl, wx.ID_ANY, style=wx.SP_3D)
        panel2 = wx.Panel(self.splitter)
        comboSizer = wx.BoxSizer(wx.VERTICAL)

        experts = allFile.getExpertLine()
        fontCombo = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.BOLD)
        comboTExpert = wx.StaticText(panel2, -1, 'Choose expert', style = wx.ALIGN_CENTER)
        comboTExpert.SetFont(fontCombo)
        self.comboExpert = wx.ComboBox(panel2, -1, choices=sorted(experts), name='Choose expert')
        self.comboExpert.SetFont(fontCombo)

        projects = []
        for pr in allFile.chosenProjects:
            projects.append(pr[1])
        for pr in allFile.unchosenProjects:
            projects.append(pr[1])
        comboTProject = wx.StaticText(panel2, -1, 'Choose project', style = wx.ALIGN_CENTER)
        comboTProject.SetFont(fontCombo)
        self.comboProject = wx.ComboBox(panel2, -1, choices=sorted(projects), name='Choose project')
        self.comboProject.SetFont(fontCombo)
        
        comboTOValue = wx.StaticText(panel2, -1, 'Risk for optimistic', style = wx.ALIGN_CENTER)
        comboTOValue.SetFont(fontCombo)
        self.valueORisk = wx.TextCtrl(panel2, -1, style = wx.ALIGN_RIGHT)
        self.valueORisk.SetFont(fontCombo)

        comboTRValue = wx.StaticText(panel2, -1, 'Risk for realistic', style = wx.ALIGN_CENTER)
        comboTRValue.SetFont(fontCombo)
        self.valueRRisk = wx.TextCtrl(panel2, -1, style = wx.ALIGN_RIGHT)
        self.valueRRisk.SetFont(fontCombo)

        comboTPValue = wx.StaticText(panel2, -1, 'Risk for pessimistic', style = wx.ALIGN_CENTER)
        comboTPValue.SetFont(fontCombo)
        self.valuePRisk = wx.TextCtrl(panel2, -1, style = wx.ALIGN_RIGHT)
        self.valuePRisk.SetFont(fontCombo)

        ButAdd = wx.Button(panel2, -1, 'Add')
        ButAdd.Bind(wx.EVT_BUTTON, lambda event: self.OnAdd(event, allFile))
        ButAdd.SetFont(fontCombo)

        comboSizer.Add(comboTExpert, 0, wx.ALIGN_CENTER|wx.ALL, 2)
        comboSizer.Add(self.comboExpert, 0, wx.EXPAND|wx.ALL, 5)
        comboSizer.Add(comboTProject, 0, wx.EXPAND|wx.ALL, 2)
        comboSizer.Add(self.comboProject, 0, wx.EXPAND|wx.ALL, 5)
        comboSizer.Add(comboTOValue, 0, wx.EXPAND|wx.ALL, 2)
        comboSizer.Add(self.valueORisk, 0, wx.EXPAND|wx.ALL, 5)
        comboSizer.Add(comboTRValue, 0, wx.EXPAND|wx.ALL, 2)
        comboSizer.Add(self.valueRRisk, 0, wx.EXPAND|wx.ALL, 5)
        comboSizer.Add(comboTPValue, 0, wx.EXPAND|wx.ALL, 2)
        comboSizer.Add(self.valuePRisk, 0, wx.EXPAND|wx.ALL, 5)
        comboSizer.Add(ButAdd, 0, wx.ALL| wx.ALIGN_CENTER, 5)

        panel2.SetSizer(comboSizer)

        RiskInfoSizer = wx.BoxSizer(wx.VERTICAL)
        panel3 = wx.Panel(self.splitter)

        riskInfo = wx.StaticText(panel3, -1, 'Risk information', style = wx.ALIGN_CENTER)
        riskInfo.SetFont(fontCombo)
        RiskInfoSizer.Add(riskInfo, 0, wx.ALL|wx.ALIGN_CENTER, 2)

        # panel32 = wx.Panel(panel3)
        # riskInfoSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.riskList = wx.ListCtrl(panel3, -1, style=wx.LC_REPORT)
        self.riskList.InsertColumn(0, 'Expert', width=150)
        self.riskList.InsertColumn(1, 'Project', width=120)
        self.riskList.InsertColumn(2, 'OptimisticAmount', width=120)
        self.riskList.InsertColumn(3, 'RealisticAmount', width=120)
        self.riskList.InsertColumn(4, 'PesimisticAmount', width=120)

        listic = allFile.getRiskInfo()
        for item in listic:
            index = self.riskList.InsertStringItem(sys.maxint, item[0])
            for colomn in range(len(item)):
                self.riskList.SetStringItem(index, colomn, item[colomn].strip())

        RiskInfoSizer.Add(self.riskList, 1, wx.EXPAND, 20)

        ButDel = wx.Button(panel3, -1, 'Delete')
        ButDel.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButDel.Bind(wx.EVT_BUTTON, lambda event: self.OnDel(event, allFile))
        RiskInfoSizer.Add(ButDel, 0, wx.ALIGN_CENTER, 10)
        panel3.SetSizer(RiskInfoSizer)

        self.splitter.SplitVertically(panel2, panel3, 250)

        panel4 = wx.Panel(pnl)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        ButOK = wx.Button(panel4, -1, 'OK')
        ButApply = wx.Button(panel4, -1, 'Apply')
        ButCancel = wx.Button(panel4, -1, 'Cancel')
        ButOK.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButApply.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButCancel.Font = wx.Font(12, wx.SWISS, wx.FONTSTYLE_ITALIC, wx.NORMAL)
        ButOK.Bind(wx.EVT_BUTTON, lambda event: self.OnOK(event, allFile))
        ButApply.Bind(wx.EVT_BUTTON, lambda event: self.OnApply(event, allFile))
        ButCancel.Bind(wx.EVT_BUTTON, self.OnCancel)
        buttonSizer.Add(ButOK, 1, wx.ALL, 5)
        buttonSizer.Add(ButApply, 1, wx.ALL, 5)
        buttonSizer.Add(ButCancel, 1, wx.ALL, 5)
        panel4.SetSizer(buttonSizer)


        vbox.Add(panel1, 0, wx.ALIGN_CENTER)
        vbox.Add(self.splitter, 0, wx.EXPAND|wx.ALIGN_CENTER,20)
        vbox.Add(panel4, 0, wx.EXPAND|wx.ALIGN_CENTER,20)

        pnl.SetSizer(vbox)
        pnl.Layout()

    def OnClose(self, e):
        self.Destroy()

    def OnAdd(self, e, allFile):

        fio = self.comboExpert.GetValue()
        project = self.comboProject.GetValue()
        optim = self.valueORisk.GetValue()
        real = self.valueRRisk.GetValue()
        pessim = self.valuePRisk.GetValue()

        gotElement = False
        for line in range(self.riskList.GetItemCount()):
            if self.riskList.GetItemText(line, 0) == fio and self.riskList.GetItemText(line, 1) == project:
                self.riskList.SetStringItem(line, 2, str(optim))
                self.riskList.SetStringItem(line, 3, str(real))
                self.riskList.SetStringItem(line, 4, str(pessim))
                gotElement = True
        if not gotElement:
            index = self.riskList.InsertStringItem(sys.maxint, fio)
            self.riskList.SetStringItem(index, 1, project)
            self.riskList.SetStringItem(index, 2, str(optim))
            self.riskList.SetStringItem(index, 3, str(real))
            self.riskList.SetStringItem(index, 4, str(pessim))

    def OnDel(self, e, allFile):
        item = self.riskList.GetFocusedItem()
        self.riskList.DeleteItem(item)


    def OnOK(self, e, allFile):
        self.OnApply(e, allFile)
        self.Destroy()

    def OnApply(self, e, allFile):
        del allFile.risks
        allFile.risks = []
        for line in range(self.riskList.GetItemCount()):
            expert = (self.riskList.GetItemText(line, 0)).split()
            project = allFile.IDOfProject(self.riskList.GetItemText(line, 1))
            optimistic = self.riskList.GetItemText(line, 2)
            realistic = self.riskList.GetItemText(line, 3)
            pessimistic = self.riskList.GetItemText(line, 3) + '\r\n'
            item = (expert[0], project, optimistic, realistic, pessimistic)
            allFile.risks.append(item)
            self.comboExpert.Value = ''
            self.comboProject.Value = ''
            self.valueORisk.Value = ''
            self.valuePRisk.Value = ''
            self.valueRRisk.Value = ''

    def OnCancel(self, e):
        self.Destroy()