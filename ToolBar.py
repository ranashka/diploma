# -*- coding: utf-8 -*-
'''
ProgRa investment
In this code example, we create main window.

author: Natalia Radziuk
last modified: February 2014
'''

import wx
import Experts, Projects, OpenSave, Risks
import NPV

class ToolBar:
    def __init__(self, parent):
        self.parent = parent
        self.toolbar = self.parent.CreateToolBar(wx.TB_3DBUTTONS|wx.TB_TEXT)
        self.Tools()
        self.toolbar.Realize()
        self.parent.contentNotSaved = True

    def Tools(self):
        ID_NEW = wx.ID_ANY
        ID_OPEN = wx.ID_ANY
        ID_SAVE = wx.ID_ANY
        ID_EXPERTS = wx.ID_ANY
        newProject = self.toolbar.AddLabelTool(ID_NEW, '', wx.Bitmap('icons/add.png'), shortHelp = 'new project')
        self.parent.Bind(wx.EVT_TOOL, self.OnNew,newProject)
        open = self.toolbar.AddLabelTool(ID_OPEN, '', wx.Bitmap('icons/open.png'), shortHelp = 'open project')
        self.parent.Bind(wx.EVT_TOOL, self.OnOpen, open)
        save = self.toolbar.AddLabelTool(ID_SAVE, '', wx.Bitmap('icons/save.png'), shortHelp = 'save project')
        self.parent.Bind(wx.EVT_TOOL, self.OnSave, save)
        self.toolbar.AddSeparator()
        experts = self.toolbar.AddLabelTool(ID_EXPERTS, '',wx.Bitmap('icons/expert.bmp'), shortHelp = 'add expert information')
        self.parent.Bind(wx.EVT_TOOL, self.DoExperts, experts)
        projects = self.toolbar.AddLabelTool(wx.ID_ANY, '',wx.Bitmap('icons/project.bmp'), shortHelp = 'add project information')
        self.parent.Bind(wx.EVT_TOOL, self.DoProjects, projects)
        risks = self.toolbar.AddLabelTool(wx.ID_ANY, '',wx.Bitmap('icons/risk.png'), shortHelp = 'add risk information')
        self.parent.Bind(wx.EVT_TOOL, self.DoRisks, risks)
        self.toolbar.AddSeparator()
        npv = self.toolbar.AddLabelTool(wx.ID_ANY, '', wx.Bitmap('icons/npv.bmp'))
        self.parent.Bind(wx.EVT_TOOL, self.OnNPV, npv)
        irr = self.toolbar.AddLabelTool(wx.ID_ANY, '', wx.Bitmap('icons/irr.bmp'))
        self.parent.Bind(wx.EVT_TOOL, self.OnIRR, irr)
        pi = self.toolbar.AddLabelTool(wx.ID_ANY, '', wx.Bitmap('icons/pi.bmp'))
        self.parent.Bind(wx.EVT_TOOL, self.OnPI, pi)
        pp = self.toolbar.AddLabelTool(wx.ID_ANY, '', wx.Bitmap('icons/pp.bmp'))
        self.parent.Bind(wx.EVT_TOOL, self.OnPP, pp)

    def DoRisks(self, e):
        Risks.RisksDialog(self.parent.allFile).Show()

    def DoExperts(self, e):
        Experts.Experts(self.parent.allFile, self.parent.panel).Show()

    def DoProjects(self, e):
        Projects.ProjectInfoDialog(self.parent.allFile, self.parent.panel).Show()

    def OnOpen(self, e):
        path = OpenSave.OnSaveAs(self.parent, e)
        self.parent.allFile.writeFile(path)
        path = OpenSave.OnOpen(self.parent, e)
        self.parent.OnOpen(path)

    def OnSave(self, e):
        path = OpenSave.OnSaveAs(self.parent, e)
        self.parent.allFile.writeFile(path)

    def OnNew(self, e):
        path = OpenSave.OnSaveAs(self.parent, e)
        self.parent.allFile.writeFile(path)
        self.parent.OnNew(e)

    def OnNPV(self, e):
        NPV.NPV(self.parent.allFile).Show()

    def OnIRR(self, e):
        self.parent.allFile.buildIRR()

    def OnPP(self, e):
        self.parent.allFile.buildPP()

    def OnPI(self, e):
        self.parent.allFile.buildPI()